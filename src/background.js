var browser = require("webextension-polyfill");

import { getWebsiteFromName } from './helpers/getWebsiteFromName'


browser.runtime.onMessage.addListener(async (msg, sender) => {
	try {
		
		const websiteName = msg && msg.getCookies && msg.getCookies.websiteName
	
		const cookiesList =  getWebsiteFromName(websiteName).cookies
		const cookies = await browser.cookies.getAll({})
		const matchingCookies = cookiesList.map((cookie) => cookies.filter((c) => c.name === cookie.name && c.domain === cookie.domain)[0])
		
		console.log('-> matchingCookies', matchingCookies) // <--- *** here you see your li_at cookie *** //
		
	} catch(e) {
		console.error(e)
	}
});